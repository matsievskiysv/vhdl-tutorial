library ieee;
use ieee.std_logic_1164.all;

entity shift_register is

  generic (
    WIDTH    : positive   := 8;
    POLARITY : std_ulogic := '0';
    LSB      : boolean    := false);

  port (
    clk, rst, data_in, latch : in  std_ulogic;
    data_out                 : out std_ulogic_vector(WIDTH-1 downto 0));

end entity shift_register;

architecture rtl of shift_register is
  signal shift_reg : std_ulogic_vector(WIDTH-1 downto 0) := (others => '0');
begin  -- architecture rtl

  seq1 : process (clk, rst) is
  begin  -- process comb
    if rst = '1' then
      shift_reg <= (others => '0');
    elsif clk'event and clk = POLARITY then
      if LSB then
        shift_reg(shift_reg'high downto 1) <= shift_reg(shift_reg'high-1 downto 0);
        shift_reg(0)                       <= data_in;
      else
        shift_reg(shift_reg'high-1 downto 0) <= shift_reg(shift_reg'high downto 1);
        shift_reg(shift_reg'high)            <= data_in;
      end if;

    end if;

  end process seq1;

  data_out <= shift_reg;

end architecture rtl;
