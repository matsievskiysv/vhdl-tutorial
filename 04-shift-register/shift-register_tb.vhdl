library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top_tb is

end entity top_tb;

architecture top_tb of top_tb is
  constant TICK                     : time                                := 0.5 us;
  constant WIDTH                    : positive                            := 5;
  signal clk, rst, data_in          : std_ulogic                          := '0';
  signal data_out_lsb, data_out_msb : std_ulogic_vector(WIDTH-1 downto 0) := (others => '0');

  component shift_register is

    generic (
      WIDTH    : positive   := 8;
      POLARITY : std_ulogic := '0';
      LSB      : boolean    := false);

    port (
      clk, rst, data_in : in  std_ulogic;
      data_out          : out std_ulogic_vector(WIDTH-1 downto 0));

  end component shift_register;


begin  -- architecture top_tb

  shift1 : component shift_register
    generic map (
      WIDTH    => WIDTH,
      POLARITY => '1',
      LSB      => true)
    port map (
      clk      => clk,
      rst      => rst,
      data_in  => data_in,
      data_out => data_out_lsb);

  shift2 : component shift_register
    generic map (
      WIDTH    => WIDTH,
      POLARITY => '1',
      LSB      => false)
    port map (
      clk      => clk,
      rst      => rst,
      data_in  => data_in,
      data_out => data_out_msb);

  clock : process is
  begin  -- process clk
    wait for TICK;
    clk <= not clk;
  end process clock;

  process is
  begin  -- process
    wait for TICK/2;
    data_in <= '1';
    wait for TICK;
    data_in <= '0';
    wait for TICK*2;
    data_in <= '1';
    wait for TICK*2;
    data_in <= '0';
    wait for TICK;
    data_in <= '1';
    wait for TICK*3;
    data_in <= '0';
    wait for TICK*5;
    data_in <= '1';
    wait for TICK;
    rst     <= '1';
    wait for TICK;
    rst     <= '0';
    wait for TICK/2;
  end process;


end architecture top_tb;
