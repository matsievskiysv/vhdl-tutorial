library ieee;
use ieee.std_logic_1164.all;

entity top_tb is

end entity top_tb;

architecture top_tb of top_tb is

  component multiplexor is

    port (
      clk, rst         : in  std_ulogic;
      input_1, input_2 : in  std_ulogic;
      output           : out std_ulogic;
      sel              : in  integer range 1 to 2);

  end component multiplexor;


  constant TICK_CLK                : time                 := 5 us;
  constant TICK_RST                : time                 := 231 us;
  constant TICK_IN1                : time                 := 150 us;
  constant TICK_IN2                : time                 := 70 us;
  constant TICK_SEL                : time                 := 2 us;
  signal in1, in2, clk             : std_ulogic           := '0';
  signal rst                       : std_ulogic           := '1';
  signal output1, output2, output3 : std_ulogic           := '0';
  signal sel                       : integer range 1 to 2 := 1;

begin  -- architecture top_tb

  mult_async_async : component multiplexor
    port map (
      clk     => clk,
      rst     => rst,
      input_1 => in1,
      input_2 => in2,
      output  => output1,
      sel     => sel);

  mult_sync_async : component multiplexor
    port map (
      clk     => clk,
      rst     => rst,
      input_1 => in1,
      input_2 => in2,
      output  => output2,
      sel     => sel);

  mult_sync_sync : entity work.multiplexor(sync_sync)
    port map (
      clk     => clk,
      rst     => rst,
      input_1 => in1,
      input_2 => in2,
      output  => output3,
      sel     => sel);

  process is
  begin  -- process
    wait for TICK_IN1;
    in1 <= not in1;
  end process;

  process is
  begin  -- process
    wait for TICK_IN2;
    in2 <= not in2;
  end process;

  process is
  begin  -- process
    wait for TICK_CLK;
    clk <= not clk;
  end process;

  process is
  begin  -- process
    wait for TICK_RST;
    rst <= not rst;
  end process;

  process is
  begin  -- process
    sel <= 1;
    wait for TICK_SEL;
    sel <= 2;
    wait for TICK_SEL;
  end process;

end architecture top_tb;

configuration conf of top_tb is

for top_tb
  for mult_async_async : multiplexor
    use entity work.multiplexor(async_async);
  end for;
  for mult_sync_async : multiplexor
    use entity work.multiplexor(sync_async);
  end for;
end for;

end configuration conf;
