library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top_tb is

end entity top_tb;

architecture top_tb of top_tb is

  constant TICK       : time    := 3 us;
  constant TICK_COUNT : time    := 7 us;
  constant IN_COUNT   : integer := 3;
  subtype sel_range is integer range 1 to IN_COUNT;
  subtype count_range is integer range 0 to 2**IN_COUNT-1;

  signal input    : std_ulogic_vector(IN_COUNT-1 downto 0) := (others => '0');
  signal clk, rst : std_ulogic                             := '1';
  signal output   : std_ulogic                             := '0';
  signal sel      : sel_range                              := 1;

begin  -- architecture top_tb

  mult_sync_sync : entity work.multiplexor(rtl)
    port map (
      clk    => clk,
      rst    => rst,
      input  => input,
      output => output,
      sel    => sel);

  process is
  begin
    clk <= not clk;
    wait for TICK;
  end process;

  process is
    variable in_counter  : count_range := 0;
    variable sel_counter : sel_range   := 1;
  begin
    input <= std_logic_vector(to_unsigned(in_counter, input'length));
    wait for TICK_COUNT;
    if in_counter = 2**IN_COUNT-1 then
      in_counter := 0;
      rst        <= '0';
      wait for TICK_COUNT;
      rst        <= '1';
    else
      in_counter := in_counter + 1;
    end if;
    if sel_counter = IN_COUNT then
      sel_counter := 1;
    else
      sel_counter := sel_counter + 1;
    end if;
    sel <= sel_counter;
    wait for TICK_COUNT/2;
  end process;

end architecture top_tb;
