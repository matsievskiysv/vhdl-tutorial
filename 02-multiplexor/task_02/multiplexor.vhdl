library ieee;
use ieee.std_logic_1164.all;

entity multiplexor is

  port (
    clk, rst : in  std_ulogic;
    input    : in  std_ulogic_vector(2 downto 0);
    output   : out std_ulogic;
    sel      : in  integer range 1 to 3);

end entity multiplexor;

architecture rtl of multiplexor is

begin  -- architecture rtl

  process (clk, rst) is
  begin  -- process
    if rst = '0' then                   -- asynchronous reset (active low)
      output <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      -- заполнить
      output <= '0';
    end if;
  end process;

end architecture rtl;
