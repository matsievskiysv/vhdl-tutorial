library ieee;
use ieee.std_logic_1164.all;

entity multiplexor is

  port (
    clk, rst         : in  std_ulogic;
    input_1, input_2 : in  std_ulogic;
    output           : out std_ulogic;
    sel              : in  integer range 1 to 2);

end entity multiplexor;

architecture async_async of multiplexor is

begin  -- architecture async_async

  output <= '0' when rst = '0' else
            input_1 when rst = '1' and sel = 1 else
            input_2 when rst = '1' and sel = 2 else
            '0';

end architecture async_async;

architecture sync_async of multiplexor is

begin  -- architecture sync_async

  process (clk, rst) is
  begin  -- process
    if rst = '0' then                   -- asynchronous reset (active low)
      output <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      case sel is
        when 1 => output <= input_1;
        when 2 => output <= input_2;
      end case;
    end if;
  end process;

end architecture sync_async;

architecture sync_sync of multiplexor is

begin  -- architecture sync_sync

  process (clk) is
  begin  -- process
    if clk'event and clk = '1' then     -- rising clock edge
      if rst = '0' then                 -- synchronous reset (active low)
        output <= '0';
      else
        if sel = 1 then
          output <= input_1;
        elsif sel = 2 then
          output <= input_2;
        else
          output <= '0';
        end if;
      end if;
    end if;
  end process;

end architecture sync_sync;
