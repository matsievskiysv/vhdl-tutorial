library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top_tb is

end entity top_tb;

architecture top_tb of top_tb is

  constant TICK       : time    := 3 us;
  constant TICK_COUNT : time    := 7 us;
  constant OUT_COUNT  : integer := 3;
  subtype sel_range is integer range 0 to OUT_COUNT-1;

  signal input    : std_ulogic                              := '0';
  signal clk, rst : std_ulogic                              := '1';
  signal output   : std_ulogic_vector(OUT_COUNT-1 downto 0) := (others => '0');
  signal sel      : sel_range                               := 0;

begin  -- architecture top_tb

  mult_sync_sync : entity work.multiplexor(rtl)
    generic map (
      OUTPUT_COUNT => OUT_COUNT)
    port map (
      clk    => clk,
      rst    => rst,
      input  => input,
      output => output,
      sel    => sel);

  process is
  begin
    clk <= not clk;
    wait for TICK;
  end process;

  process is
    variable sel_counter : sel_range := 0;
  begin
    input <= not input;
    wait for TICK_COUNT;
    if sel_counter = OUT_COUNT-1 then
      sel_counter := 0;
      rst         <= '0';
      wait for TICK_COUNT;
      rst         <= '1';
    else
      sel_counter := sel_counter + 1;
    end if;
    sel <= sel_counter;
    wait for TICK_COUNT/2;
  end process;

end architecture top_tb;
