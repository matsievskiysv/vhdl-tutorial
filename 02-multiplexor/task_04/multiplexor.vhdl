library ieee;
use ieee.std_logic_1164.all;

entity multiplexor is
  generic (
    OUTPUT_COUNT : integer);
  port (
    clk, rst : in  std_ulogic;
    output   : out std_ulogic_vector(OUTPUT_COUNT-1 downto 0);
    input    : in  std_ulogic;
    sel      : in  integer range 0 to OUTPUT_COUNT-1);

end entity multiplexor;

architecture rtl of multiplexor is

begin  -- architecture rtl

  process (clk, rst) is
  begin  -- process
    if rst = '0' then                   -- asynchronous reset (active low)
      output <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      -- заполнить
      output <= (others => '0');
    end if;
  end process;

end architecture rtl;
