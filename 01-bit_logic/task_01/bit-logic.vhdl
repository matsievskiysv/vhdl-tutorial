library ieee;
use ieee.std_logic_1164.all;

entity logic_element is
  port (
    i_0, i_1, i_2 : in  std_ulogic;
    o_0, o_1      : out std_ulogic);
end entity logic_element;

architecture rtl of logic_element is

begin

  -- заполнить
  o_0 <= '0';
  o_1 <= '0';

end architecture rtl;
