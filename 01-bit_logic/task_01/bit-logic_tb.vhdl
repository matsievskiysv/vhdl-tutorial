library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top_tb is

end entity top_tb;

architecture top_tb of top_tb is
  constant TICK      : time                                    := 1 us;
  constant IN_COUNT  : integer                                 := 3;
  constant OUT_COUNT : integer                                 := 2;
  signal input       : std_ulogic_vector(IN_COUNT-1 downto 0)  := (others => '0');
  signal output      : std_ulogic_vector(OUT_COUNT-1 downto 0) := (others => '0');

begin  -- architecture top_tb

  logic_element1 : entity work.logic_element(rtl)
    port map (
      i_0 => input(0),
      i_1 => input(1),
      i_2 => input(2),
      o_0 => output(0),
      o_1 => output(1));

  process is
    variable counter : integer range 0 to 2**IN_COUNT-1 := 0;
  begin
    input <= std_logic_vector(to_unsigned(counter, input'length));
    wait for TICK;
    if counter = 2**IN_COUNT-1 then
      counter := 0;
    else
      counter := counter + 1;
    end if;
  end process;

end architecture top_tb;
