library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top_tb is

end entity top_tb;

architecture top_tb of top_tb is
  constant TICK  : time                          := 1 us;
  signal input   : std_ulogic_vector(1 downto 0) := "00";
  signal and_out : std_ulogic                    := '0';
  signal or_out  : std_ulogic                    := '0';
  signal xor_out : std_ulogic                    := '0';

  component logic_and is
    port (
      input_1 : in  std_ulogic;
      input_2 : in  std_ulogic;
      output  : out std_ulogic);
  end component logic_and;

  component logic_or is
    port (
      input_1 : in  std_ulogic;
      input_2 : in  std_ulogic;
      output  : out std_ulogic);
  end component logic_or;

begin  -- architecture top_tb

  logic_and1 : component logic_and
    port map (
      input_1 => input(0),
      input_2 => input(1),
      output  => and_out);

  logic_or1 : component logic_or
    port map (
      input_1 => input(0),
      input_2 => input(1),
      output  => or_out);

  logic_xor1 : entity work.logic_xor
    port map (
      input_1 => input(0),
      input_2 => input(1),
      output  => xor_out);

  process is
  begin
    input   <= "00";
    wait for TICK;
    input   <= "01";
    wait for TICK;
    input   <= "10";
    wait for TICK;
    input   <= "11";
    wait for TICK;
  end process;

end architecture top_tb;
