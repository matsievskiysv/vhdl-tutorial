# Язык описания логических схем VHDL

VHDL является языком описания логических схем (hardware description language, HDL).
Языки HDL используются для программирования ПЛИС (FPGA) и верификации специализированных интегральных схем (ASIC). Не смотря на внешнее сходство, программирование на HDL сильно отличается от программирования на языках, предназначенных для выполнения на микроконтроллерах и микропроцессорах:

* Выражения языка описывают логические устройства и выполняются параллельно
* Переключение логического уровня связано с переходными процессами в устройстве и занимает некоторое время

Во многом программирование на языках HDL имеет большее сходство со схемотехникой. Задачей программиста HDL является соединение устройств (`entity`) для получения составного устройства с необходимой логикой работы.

## Элементы языка VHDL

Составные части устройств в языке VHDL -- это элементы `entity`. Каждый элемент `entity` может иметь входы и выходы (или не иметь их) и описывает, как данный элемент может быть подключён к другим элементам.
Каждому элементу `entity` может соответствовать любое количество элементов `architecture`, описывающих внутреннюю логику работы устройства.

Здесь уместна аналогия со схемотехникой: элемент `entity` описывает корпус устройства и функциональное назначение его выводов. Элемент `architecture` является внутренним наполнением корпуса. В одном и том же корпусе с одинаковым расположением контактов могут быть выполнены различные устройства.
К колодке можно подключить любое устройство, при условии совпадения типа корпуса и его разпиновки.

![socket](images/socket.jpg)
![ic](images/ic.jpg)

### Элемент (Entity)

Элемент `entity` описывает названия портов, их тип и направление передачи данных.

```vhdl
entity nand_gate is
port(
	a : in std_logic;
	b : in std_logic;
	c : in std_logic;
	z : out std_logic);
end entity nand_gate;
```

![entity](images/entity.png)

### Архитектура (Architecture)

`architecture` описывает внутреннюю логику работы устройства. Определённые в `entity` сигналы автоматически доступны для манипуляций в `archtecture`. При этом требуется соблюдение направления движения данных в устройстве (сигналы должные входить из входа и выходить в выход). Возможно использование внутренних сигналов и переменных для реализации необходимого алгоритма работы устройства. Архитектура устройства может содержать в себе другие `entity`.

```vhdl
architecture arch1 of nand_gate is
begin
  z <= a;
end architecture arch1;
```

## Типы сигналов

VHDL различает 2 типа переменных: сигналы и переменные. Компиляторы VHDL используют тип переменных для более эффективного синтеза схем для ПЛИС.

### Сигналы

Продолжая аналогию со схемотехникой, сигнал описывает контакт или группу контактов (шину). Определённые в `entity` сигналы автоматически доступны для манипуляций в `archtecture`. При этом требуется соблюдение направления движения данных в устройстве (сигналы должные входить из входа и выходить в выход). Не следует использовать двунаправленные сигналы `inout` без необходимости.

Внутренний сигнал объявляется для всей архитектуры до ключевого слова `begin`; имеет название, тип и начальное значение. Внутренние сигналы являются двунаправленными. Подключение к сигналу осуществляется при помощи оператора `<:`.

```vhdl
architecture arch1 of nand_gate is
  signal internal : std_logic := '0';
begin
  internal <= (a or b) and c;
  z <= internal;
end architecture arch1;
```

Следует следить за тем, чтобы уровень сигнала одновременно задавался только из одного места (напоминаем, строки кода выполняются одновременно и должны интерпретироваться как физически подключённые провода, а не выполняемые функции):

```vhdl
internal <= a; -- сигнал задаётся одновременно с b
internal <= b; -- сигнал задаётся одновременно с a
```

Способы переключения источников сигналов будут рассмотрены в последующих лабораторных работах.

### Переменные

Переменные используются в процедурах (`procedure`) так же как в обычных языках программирования. Физически они представляют собой ячейки памяти -- регистры. Подробнее они будут рассмотрены в последующих лабораторных работах.

## Типы данных

В VHDL существует набор встроенных типов данных, а также есть возможность на их основе задавать новые типы данных.

### Boolean

Логический тип данных.

```vhdl
variable done: boolean:= false;
signal enable: boolean:= true;
```

### Bit

Логический тип данных. Отличается от `boolean` только представлением.

```vhdl
variable done: bit:= 0;
signal enable: bit:= 1;
```

### Integer

Целочисленная переменная в диапазоне $\left(-2^{31};2^{31}\right)$. Обычно для работы выбирается ограниченный диапазон при помощи `range`.
В VHDL встроены подтипы типа `integer`:

* `natural`: $\left(0;2^{31}\right)$
* `positive`: $\left(1;2^{31}\right)$

```vhdl
variable VALUE: natural := 2;
```

### Integer range

Часто для работы требуется только ограниченный диапазон значений целочисленной переменной. Для определения диапазона используется выражение `range .. to ..`.

```vhdl
variable new_integer is integer range 50 to 150;
```

### `std_logic` и `std_ulogic`

Сложный тип данных, описывающий сигналы физических логических устройств. Большая часть значений сигнала не может быть синтезирована в ПЛИС, и играет роль только в программной симуляции устройства.

| Значение  | Описание  | Реализуемо в ПЛИС  |
|:--|:--|:--|
| `U`  | Неизвестно. Сигнал ещё не задан  | Нет  |
| `X`  | Неизвестно. Сигнал не может быть определён  | Нет  |
| `0`  | Логический ноль  | Да  |
| `1`  | Логическая единица  | Да  |
| `Z`  | Высокоомный вход  | Да  |
| `W`  | Сигнал не может быть определён  | Нет  |
| `L`  | Слабо подтянутый к логическому нулю сигнал  | Нет  |
| `H`  | Слабо подтянутый к логической единице сигнал  | Нет  |
| `-`  | Не важно  | Да (в мультиплексорах)  |

```vhdl
signal signal1 : std_logic := '0';
signal signal2 : std_ulogic := '1';
```

`std_logic` отличается от `std_ulogic` тем, что позволяет подключать к одному сигналу несколько источников. Результирующий сигнал при этом определяется таблицей.

|   | `U`  | `X`  | `0`  | `1`  | `Z`  | `W`  | `L`  | `H`  | `-` |
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| `U`  | `U`  | `U`  | `U`  | `U`  | `U`  | `U`  | `U`  | `U`  | `U`  |
| `X`  | `U`  | `X`  | `X`  | `X`  | `X`  | `X`  | `X`  | `X`  | `X`  |
| `0`  | `U`  | `X`  | `0`  | `X`  | `0`  | `0`  | `0`  | `0`  | `X`  |
| `1`  | `U`  | `X`  | `X`  | `1`  | `1`  | `1`  | `1`  | `1`  | `X`  |
| `Z`  | `U`  | `X`  | `0`  | `1`  | `Z`  | `W`  | `L`  | `H`  | `X`  |
| `W`  | `U`  | `X`  | `0`  | `1`  | `W`  | `W`  | `W`  | `W`  | `X`  |
| `L`  | `U`  | `X`  | `0`  | `1`  | `L`  | `W`  | `L`  | `W`  | `X`  |
| `H`  | `U`  | `X`  | `0`  | `1`  | `H`  | `W`  | `W`  | `H`  | `X`  |
| `-`  | `U`  | `X`  | `X`  | `X`  | `X`  | `X`  | `X`  | `X`  | `X`  |

Обычно данные типы сигналов используются для внешних сигналов устройства, определённых в `entity`.

### `std_logic_vector` и `std_ulogic_vector`

Данные типы сигналов определяют шины данных. Линии в шинах могут нумероваться произвольно, как от старшего бита к младшему, так и наоборот.

```vhdl
signal bus1 : std_logic_vector(7 downto 0);
signal bus2 : std_logic_vector(0 to 7);
```

Обычно данные типы сигналов используются для внешних сигналов устройства, определённых в `entity`.

### Time

Данный тип данных часто используется в симуляциях для задания задержки сигнала. Поддерживаются размерности `fs`, `ps`, `ns`, `us`, `ms`, `sec`, `min`, `hr`.

```vhdl
constant tick : time := 1 us;
wait for tick;
```

### Record

Тип `record` -- это сложный (композитный) тип данных, включающий в себя поля других типов. Он является аналогией типа `struct` в языке C.

```vhdl
type t_rec1 is record
   f1 : std_logic;
   f2 : std_logic_vector(7 downto 0);
end record t_rec1;

constant zero_rec1 : t_rec1 := (
   f1 => '0',
   f2 => (others => '0')
);
```

### Преобразование типов

Диаграмма преобразований типов изображена на рисунке ниже
![conversion](images/vhdl-type-conversions.png)

## Добавление элемента

Важным аспектом языка является добавление компонентов `entity` в `architecture`. В VHDL есть два способа добавления компонента, различающихся только удобством использования в проекте:

* `entity`: более компактный способ добавления компонента.
* `component`: более подробный способ добавления компонента. Позволяет использовать компоненты, написанные на языке Verilog, в проекте VHDL.

### Добавление `entity`

Для добавления компонента при помощи `entity` требуется:

* выбрать название (`element_name`). Оно должно быть уникальным в данной архитектуре.
* указать названия библиотеки, компонента и архитектуры (`library_name`, `entity_name` и `entity_architecture_name`).
* указать, какие сигналы компонента (`entity_signal_*`) подключены к каким сигналам архитектуры (`architecture_signal_3`).

```vhdl
element_name : entity library_name.entity_name(entity_architecture_name)
  port map (
	entity_signal_1 => architecture_signal_3,
	entity_signal_2 => architecture_signal_1,
	entity_signal_3 => architecture_signal_2);
```

### Добавление `component`

Добавление компонента при помощи `component` отличается тем, что требует полного определения компонента внутри архитектуры до ключевого слова `begin`.

```vhdl
architecture outher_architecture of outer_entity is
  signal in1, in2, out : std_ulogic := '0';

  component inner_entity is
    port (
      input_1 : in  std_ulogic;
      input_2 : in  std_ulogic;
      output  : out std_ulogic);
  end component inner_entity;

begin

  comp1 : component inner_entity
    port map (
      input_1 => in1,
      input_2 => in2,
      output  => out);

end architecture outher_architecture;
```

## Тестовый стенд (Test bench)

Для тестирования разработанных устройств используют тестовые стенды: компоненты VHDL, включающие в себя тестируемый компонент с подключёнными к нему сигналами. В рамках тестов задаются входные сигналы и проверяется соответствие выходных сигналов их ожидаемым уровням. Это можно делать визуально, изучая симулированные сигналы устройства, или при помощи команды `assert`.

# Задание

1. Для представленных в примере компонентов построить полные таблицы истинности
2. В файле [task_01/bit-logic.vhdl](./task_01/bit-logic.vhdl) написать архитектуру элемента `logic_element` для представленных ниже выражений и построить их таблицы истинности (`i*` -- входные сигналы, `o*` -- выходные сигналы)
   * $$\begin{cases}o_0 = i_0 \lor i_1 \land i_2 \\ o_1 = \neg i_0 \oplus i_2 \end{cases}$$
   * $$\begin{cases}o_0 = i_0 \land i_1 \land i_2 \\ o_1 = \neg i_0 \oplus i_2 \end{cases}$$
   * $$\begin{cases}o_0 = i_0 \lor i_1 \lor i_2 \\ o_1 = \neg i_0 \oplus i_2 \end{cases}$$
   * $$\begin{cases}o_0 = i_0 \lor i_1 \oplus i_2 \\ o_1 = \neg i_0 \oplus i_2 \end{cases}$$
   * $$\begin{cases}o_0 = i_0 \land i_1 \oplus i_2 \\ o_1 = \neg i_0 \oplus i_2 \end{cases}$$
   * $$\begin{cases}o_0 = i_0 \lor i_1 \land i_2 \\ o_1 = i_0 \oplus \neg i_2 \end{cases}$$
   * $$\begin{cases}o_0 = i_0 \land i_1 \land i_2 \\ o_1 = \neg i_0 \land i_2 \end{cases}$$
   * $$\begin{cases}o_0 = i_0 \lor i_1 \lor i_2 \\ o_1 = \neg i_0 \lor i_2 \end{cases}$$
   * $$\begin{cases}o_0 = i_0 \lor i_1 \oplus i_2 \\ o_1 = \neg \left( i_0 \lor i_2 \right) \end{cases}$$
   * $$\begin{cases}o_0 = i_0 \land i_1 \oplus i_2 \\ o_1 = \neg i_0 \land \neg i_2 \end{cases}$$
