library ieee;
use ieee.std_logic_1164.all;

entity logic_and is
  port (
    input_1 : in  std_ulogic;
    input_2 : in  std_ulogic;
    output  : out std_ulogic);
end entity logic_and;

architecture rtl of logic_and is

begin  -- architecture rtl

  output <= input_1 and input_2;

end architecture rtl;

------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity logic_or is
  port (
    input_1 : in  std_ulogic;
    input_2 : in  std_ulogic;
    output  : out std_ulogic);
end entity logic_or;

architecture rtl of logic_or is

begin  -- architecture rtl

  output <= input_1 or input_2;

end architecture rtl;

------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity logic_xor is
  port (
    input_1 : in  std_ulogic;
    input_2 : in  std_ulogic;
    output  : out std_ulogic);
end entity logic_xor;

architecture rtl of logic_xor is

begin  -- architecture rtl

  output <= input_1 xor input_2;

end architecture rtl;
