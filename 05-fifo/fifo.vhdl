library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity fifo is

  generic (
    WIDTH    : positive   := 8;
    POLARITY : std_ulogic := '0';
    DEPTH    : positive   := 8);

  port (
    data_write : in  std_ulogic_vector(WIDTH-1 downto 0);
    clk_write  : in  std_ulogic;
    full       : out std_ulogic;
    data_read  : out std_ulogic_vector(WIDTH-1 downto 0);
    clk_read   : in  std_ulogic;
    empty      : out std_ulogic);

end entity fifo;

architecture rtl of fifo is
  type mem_t is array (0 to DEPTH-1) of std_ulogic_vector(WIDTH-1 downto 0);
  signal mem           : mem_t                       := (others => (others => '0'));
  signal full_flag     : boolean                     := false;
  signal empty_flag    : boolean                     := true;
  signal read_pointer  : positive range 0 to DEPTH-1 := 0;
  signal write_pointer : positive range 0 to DEPTH-1 := 0;
begin  -- architecture rtl

  full  <= '0' when full_flag  else '1';
  empty <= '0' when empty_flag else '1';

  seq_write : process (clk_write) is
  begin  -- process seq_write
    if clk_write'event and clk_write = POLARITY and not full_flag then  -- rising clock edge
      mem(write_pointer) <= data_write;
      if write_pointer = DEPTH - 1 then
        write_pointer <= 0;
      else
        write_pointer <= write_pointer + 1;
      end if;
    end if;
  end process seq_write;

  seq_read : process (clk_read) is
  begin  -- process seq_read
    if clk_read'event and clk_read = POLARITY and not empty_flag then  -- rising clock edge
      data_read <= mem(read_pointer);
      if read_pointer = DEPTH - 1 then
        read_pointer <= 0;
      else
        read_pointer <= read_pointer + 1;
      end if;
    end if;
  end process seq_read;

  flag_proc : process (read_pointer, write_pointer) is
    variable read_pointer_previous  : positive range 0 to DEPTH-1 := 0;
    variable write_pointer_previous : positive range 0 to DEPTH-1 := 0;
  begin  -- process
    if read_pointer /= read_pointer_previous then
      empty_flag <= read_pointer = write_pointer;
      full_flag  <= false;
    end if;
    if write_pointer /= write_pointer_previous then
      empty_flag <= false;
      full_flag  <= read_pointer = write_pointer;
    end if;
    read_pointer_previous  := read_pointer;
    write_pointer_previous := write_pointer;
  end process;

end architecture rtl;
