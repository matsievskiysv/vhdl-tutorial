library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity fifo is

  generic (
    WIDTH    : positive   := 8;
    POLARITY : std_ulogic := '0';
    DEPTH    : positive   := 8);

  port (
    data_write   : in  std_ulogic_vector(WIDTH-1 downto 0);
    clk_write    : in  std_ulogic;
    full         : out std_ulogic;
    almost_full  : out std_ulogic;
    data_read    : out std_ulogic_vector(WIDTH-1 downto 0);
    clk_read     : in  std_ulogic;
    empty        : out std_ulogic;
    almost_empty : out std_ulogic);

end entity fifo;

architecture rtl of fifo is
  type mem_t is array (0 to DEPTH-1) of std_ulogic_vector(WIDTH-1 downto 0);
  signal mem : mem_t := (others => (others => '0'));
begin  -- architecture rtl

  -- заполнить
  mem <= (others => (others => '0'));

end architecture rtl;
