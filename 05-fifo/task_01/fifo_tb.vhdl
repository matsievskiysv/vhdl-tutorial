library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top_tb is

end entity top_tb;

architecture top_tb of top_tb is
  constant TICK                              : time                                := 0.5 us;
  constant WIDTH                             : positive                            := 8;
  constant DEPTH                             : positive                            := 8;
  signal clk_write, clk_read                 : std_ulogic                          := '0';
  signal full_flag, empty_flag               : std_ulogic                          := '0';
  signal almost_full_flag, almost_empty_flag : std_ulogic                          := '0';
  signal input, output                       : std_ulogic_vector(WIDTH-1 downto 0) := (others => '0');

begin  -- architecture top_tb

  fifo1 : entity work.fifo(rtl)
    generic map (
      WIDTH    => WIDTH,
      POLARITY => '1',
      DEPTH    => DEPTH)
    port map (
      data_write   => input,
      clk_write    => clk_write,
      full         => full_flag,
      almost_full  => almost_full_flag,
      data_read    => output,
      clk_read     => clk_read,
      empty        => empty_flag,
      almost_empty => almost_empty_flag);

  process is
  begin  -- process
    for i in 0 to DEPTH+1 loop
      input     <= std_ulogic_vector(to_unsigned(i, input'length));
      clk_write <= '1';
      wait for TICK;
      clk_write <= '0';
      wait for TICK;
    end loop;  -- i
    for i in 0 to DEPTH/2 loop
      clk_read <= '1';
      wait for TICK;
      clk_read <= '0';
      wait for TICK;
    end loop;  -- i
    for i in DEPTH+1 downto 1 loop
      input     <= std_ulogic_vector(to_unsigned(i, input'length));
      clk_write <= '1';
      wait for TICK;
      clk_write <= '0';
      wait for TICK;
    end loop;  -- i
    for i in 1 to DEPTH+2 loop
      clk_read <= '1';
      wait for TICK;
      clk_read <= '0';
      wait for TICK;
    end loop;  -- i
  end process;


end architecture top_tb;
