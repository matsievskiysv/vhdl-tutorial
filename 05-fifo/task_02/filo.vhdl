library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity filo is

  generic (
    WIDTH : positive := 8;
    DEPTH : positive := 8);

  port (
    data  : inout std_logic_vector(WIDTH-1 downto 0);
    clk   : in    std_ulogic;
    we    : in    std_ulogic;
    full  : out   std_ulogic;
    empty : out   std_ulogic);

end entity filo;

architecture rtl of filo is
  type mem_t is array (0 to DEPTH-1) of std_ulogic_vector(WIDTH-1 downto 0);
  signal mem           : mem_t                      := (others => (others => '0'));
  signal data_internal : std_logic_vector(WIDTH-1 downto 0);
  signal index         : integer range 0 to DEPTH-1 := 0;
begin  -- architecture rtl

  data <= data_internal when we = '0' else (others => 'Z');
  mem <= (others => (others => '0'));

  process (clk) is
    variable current : integer range 0 to DEPTH-1 := 0;
  begin  -- process
    if clk'event and clk = '1' then     -- rising clock edge
      if we = '1' then
      -- заполнить
      else
      -- заполнить
      end if;
      index <= current;
    end if;
  end process;

end architecture rtl;
