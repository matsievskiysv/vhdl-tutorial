library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top_tb is

end entity top_tb;

architecture top_tb of top_tb is
  constant TICK                : time                               := 0.5 us;
  constant WIDTH               : positive                           := 8;
  constant DEPTH               : positive                           := 8;
  signal clk, we_flag          : std_ulogic                         := '0';
  signal full_flag, empty_flag : std_ulogic                         := '0';
  signal data                  : std_logic_vector(WIDTH-1 downto 0) := (others => '0');

begin  -- architecture top_tb

  filo1 : entity work.filo(rtl)
    generic map (
      WIDTH => WIDTH,
      DEPTH => DEPTH)
    port map (
      data  => data,
      clk   => clk,
      we    => we_flag,
      full  => full_flag,
      empty => empty_flag);

  process is
  begin  -- process
    we_flag <= '1';
    for i in 0 to DEPTH+1 loop
      data <= std_logic_vector(to_unsigned(i, data'length));
      clk  <= '0';
      wait for TICK;
      clk  <= '1';
      wait for TICK;
    end loop;  -- i
    we_flag <= '0';
    data    <= (others => 'Z');
    for i in 0 to depth/2 loop
      clk <= '0';
      wait for tick;
      clk <= '1';
      wait for tick;
    end loop;  -- i
    we_flag <= '1';
    for i in depth+1 downto 1 loop
      data <= std_ulogic_vector(to_unsigned(i, data'length));
      clk  <= '0';
      wait for tick;
      clk  <= '1';
      wait for tick;
    end loop;  -- i
    we_flag <= '0';
    data    <= (others => 'Z');
    for i in 1 to depth+2 loop
      clk <= '0';
      wait for tick;
      clk <= '1';
      wait for tick;
    end loop;  -- i
  end process;


end architecture top_tb;
