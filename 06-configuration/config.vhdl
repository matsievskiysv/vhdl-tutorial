package config is

  constant WAIT1 : time := 10 us;
  constant WAIT2 : time := 2 us;
  constant WAIT3 : time := 3 us;
  constant WAIT4 : time := 4 us;

  constant MULT_INVERT : boolean := true;

end package config;
