library ieee;
use ieee.std_logic_1164.all;

entity logic is
  port (
    input_1 : in  std_ulogic;
    input_2 : in  std_ulogic;
    output  : out std_ulogic);
end entity logic;

architecture logic_and of logic is

begin  -- architecture logic

  output <= input_1 and input_2;

end architecture logic_and;

architecture logic_or of logic is

begin  -- architecture logic

  output <= input_1 or input_2;

end architecture logic_or;

architecture logic_xor of logic is

begin  -- architecture logic

  output <= input_1 xor input_2;

end architecture logic_xor;
