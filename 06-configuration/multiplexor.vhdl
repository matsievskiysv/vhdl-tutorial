library ieee;
use ieee.std_logic_1164.all;

entity multiplexor is

  generic (
    INVERT : boolean := false);

  port (
    input_1, input_2, sel : in  std_ulogic;
    output                : out std_ulogic);

end entity multiplexor;

architecture rtl of multiplexor is

begin  -- architecture rtl

  invert_sel : if INVERT generate
    with sel select
      output <=
      input_1 when '0',
      input_2 when others;
  else generate
    with sel select
      output <=
      input_1 when '1',
      input_2 when others;
  end generate invert_sel;

end architecture rtl;
