library ieee;
use ieee.std_logic_1164.all;

entity logic_wrapper is

  port (
    input_1, input_2 : in  std_ulogic;
    output           : out std_ulogic);

end entity logic_wrapper;

architecture rtl of logic_wrapper is

  component logic is
    port (
      input_1 : in  std_ulogic;
      input_2 : in  std_ulogic;
      output  : out std_ulogic);
  end component logic;

begin  -- architecture rtl

  logic1 : component logic
    port map (
      input_1 => input_1,
      input_2 => input_2,
      output  => output);

end architecture rtl;

configuration logic_and of logic_wrapper is
  for rtl
    for all : logic
      use entity work.logic(logic_and);
    end for;
  end for;
end configuration logic_and;

configuration logic_or of logic_wrapper is
  for rtl
    for all : logic
      use entity work.logic(logic_or);
    end for;
  end for;
end configuration logic_or;

configuration logic_xor of logic_wrapper is
  for rtl
    for all : logic
      use entity work.logic(logic_xor);
    end for;
  end for;
end configuration logic_xor;
