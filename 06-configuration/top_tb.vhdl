library ieee;
use ieee.std_logic_1164.all;

use work.config.all;

entity top_tb is

end entity top_tb;

architecture top_tb of top_tb is

  component logic_wrapper is

    port (
      input_1, input_2 : in  std_ulogic;
      output           : out std_ulogic);

  end component logic_wrapper;

  component clock is

    generic (
      TICK : time := 1 us);

    port (clk : out std_ulogic);

  end component clock;

  component multiplexor is

    generic (
      INVERT : boolean := false);

    port (
      input_1, input_2, sel : in  std_ulogic;
      output                : out std_ulogic);

  end component multiplexor;

  signal mult_sel   : std_ulogic := '0';
  signal logic_in_1 : std_ulogic := '0';
  signal mult_in_1  : std_ulogic := '0';
  signal mult_in_2  : std_ulogic := '0';
  signal int1       : std_ulogic := '0';
  signal output     : std_ulogic := '0';

begin  -- architecture top_tb

  clock1 : component clock
    port map (
      clk => mult_sel);

  clock2 : component clock
    port map (
      clk => logic_in_1);

  clock3 : component clock
    port map (
      clk => mult_in_1);

  clock4 : component clock
    port map (
      clk => mult_in_2);

  logic1 : component logic_wrapper
    port map (
      input_1 => int1,
      input_2 => logic_in_1,
      output  => output);

  mult1 : component multiplexor
    port map (
      input_1 => mult_in_1,
      input_2 => mult_in_2,
      sel     => mult_sel,
      output  => int1);

end architecture top_tb;

configuration test_conf of top_tb is

  for top_tb
    for logic1 : logic_wrapper
      use configuration work.logic_xor;
    end for;

    for clock1 : clock
      use entity work.clock
        generic map (
          TICK => WAIT1);
    end for;

    for clock2 : clock
      use entity work.clock
        generic map (
          TICK => WAIT2);
    end for;

    for clock3 : clock
      use entity work.clock
        generic map (
          TICK => WAIT3);
    end for;

    for clock4 : clock
      use entity work.clock
        generic map (
          TICK => WAIT4);
    end for;

    for mult1 : multiplexor
      use entity work.multiplexor
        generic map (
          INVERT => MULT_INVERT);
    end for;

  end for;

end configuration test_conf;
