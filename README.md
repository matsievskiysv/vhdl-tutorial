# Лабораторные работы по VHDL

## Лабораторные работы

* [Логические операции](01-bit_logic/README.md)
* [Условные операции](02-multiplexor/README.md)
* [Сумматор](03-full_adder/README.md)
* [Сдвиговый регистр](04-shift-register/README.md)
* [Буфер FIFO](05-fifo/README.md)
* [Конфигурация](06-configuration/README.md)
* [ROM память](07-rom/README.md)

## Установка необходимых программ в системе GNU/Linux

Настройка переменных.

```bash
export INSTALL_DIR=/opt
export BUILD_DIR=/tmp/build
mkdir -p $BUILD_DIR
sudo mkdir -p $INSTALL_DIR
```

### `gtkwave`

Установка `gtkwave` из репозитория:

```bash
sudo apt install gtkwave
```

### `ghdl` и `ghdl-ls`

Сборка `ghdl` VHDL компилятора и VHDL LSP сервера.

```bash
cd $BUILD_DIR
sudo apt build-dep ghdl
git clone https://github.com/ghdl/ghdl.git
pushd ghdl
git checkout v4.1.0
./configure --prefix=$INSTALL_DIR/ghdl
make -j $(nproc)
sudo make install
pip3 install --user --break-system-packages .
echo 'export PATH=$PATH':"$INSTALL_DIR/ghdl/bin" >> $HOME/.profile
popd
```

## Установка необходимых программ в системе Windows

### `cygwin`

Для работы в системе Windows сначала требуется установить программу [cygwin](https://www.cygwin.com/). Для этого скачать со страницы <https://www.cygwin.com/install.html> установщик и запустить его. Пути установки можно указать любые. Зеркало для установки можно выбрать любое, но чем ближе оно расположено, тем быстрее будет идти загрузка программ. В окне выбора дополнительных компонентов надо выбрать `View` `Full`, в графе `Search` указать `make`. В появившемся списке надо найти программу `make` и в колонке `New` указать самую последнюю версию, у которой ней отметки `Test`. Повторить манипуляции для программы `git`. По завершении установки в системе появится новая программа `Cygwin Terminal`.

В программе `Cygwin Terminal` будет открыт терминал, эмулирующий систему GNU/Linux. Требуется определить расположение его домашней папки в системе при помощи команды
```bash
cygpath -w ~
```
По этому пути рабочие файлы будут доступны в системе. Эта папка будет упоминаться далее под названием *папка сygwin*.

**Обратите внимание:**
Для копирования/вставки текста из буфера обмена в cygwin доступны горячие клавиши <kbd>Shift+Ins</kbd> и <kbd>Ctrl+Ins</kbd>.

### `gtkwave`

Для установки `gtkwave` требуется скачать архив <https://sourceforge.net/projects/gtkwave/files/gtkwave-3.3.100-bin-win64/gtkwave-3.3.100-bin-win64.zip> и распаковать его в *папку cygwin*. После этого, из терминала cygwin выполнить команду

```bash
find ~ -name gtkwave.exe -exec ln -s {} /usr/bin/gtkwave \;
```

Для проверки теперь можно выполнить команду, которая должна открывать окно программы

```bash
gtkwave
```

### `ghdl`

Для установки `ghdl` требуется скачать архив <https://github.com/ghdl/ghdl/releases/download/nightly/ghdl-MINGW32.zip> и распаковать его в *папку cygwin*. После этого, из терминала cygwin выполнить команду

```bash
find ~ -name ghdl.exe -exec ln -s {} /usr/bin/ghdl \;
```

Для проверки теперь можно выполнить команду, которая должна показывать информационное сообщение команды

```bash
ghdl -h
```

## Запуск программ

### Копирование проекта

Для копирования файлов с удалённого сервера требуется выполнить команду
```bash
git clone https://gitlab.com/matsievskiysv/vhdl-tutorial.git
cd vhdl-tutorial
```

### Обновление проекта

Файлы проекта могут обновляться. Чтобы обновить проект, требуется выполнить команду
```bash
git pull origin master
```

### Запуск программ

Для запуска программ в нужной последовательности используется программа [GNU Make](https://www.gnu.org/software/make/). В управляющем файле `Makefile` определены следующие команды:

* `make simulate`: скомпилировать проект, произвести симуляцию
* `make view`: открыть окно логического анализатора

Оба действия можно выполнить одной командой

```bash
make
```
