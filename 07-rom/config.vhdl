library ieee;
use ieee.std_logic_1164.all;

package config is

  constant DATA_WIDTH : positive := 32;
  constant ROM_SIZE   : positive := 128;

  subtype address_t is natural range 0 to ROM_SIZE-1;
  subtype result_t is std_ulogic_vector(DATA_WIDTH/2-1 downto 0);
  subtype reg_t is std_ulogic_vector(DATA_WIDTH-1 downto 0);

end package config;
