library ieee;
use ieee.std_logic_1164.all;

use work.config.all;

entity top is

end entity top;

architecture rtl of top is

  signal clk1, clk2 : std_ulogic := '0';
  signal pc         : address_t  := address_t'high-1;
  signal data       : reg_t      := (others => '0');
  signal err        : std_ulogic := '0';
  signal output     : result_t;

begin  -- architecture rtl

  clock1 : entity work.clock
    generic map (
      TICK => 1 us)
    port map (
      clk => clk1);

  clock2 : entity work.clock
    generic map (
      TICK => 2.7 us)
    port map (
      clk => clk2);

  pc1 : process (clk2) is
  begin  -- process pc1
    if clk2'event and clk2 = '1' then     -- rising clock edge
      if pc >= address_t'high then
        pc <= 0;
      else
        pc <= pc + 1;
      end if;
    end if;
  end process pc1;

  rom1 : entity work.rom
    port map (
      clk  => clk1,
      pc   => pc,
      data => data,
      err  => err);

  decoder1 : entity work.decoder
    port map (
      clk    => clk1,
      rst    => '1',
      input  => data,
      output => output);

end architecture rtl;
