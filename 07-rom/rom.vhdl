library ieee;
use ieee.std_logic_1164.all;
use std.textio.all;

use work.config.all;

entity rom is

  port (
    clk  : in  std_ulogic;
    pc   : in  address_t;
    data : out reg_t;
    err  : out std_ulogic);

end entity rom;

architecture rtl of rom is
  type mem_t is array (0 to ROM_SIZE-1) of std_ulogic_vector(DATA_WIDTH-1 downto 0);
  signal mem           : mem_t      := (others => (others => '0'));
  signal err_internal  : std_ulogic := '0';
  signal data_internal : reg_t;
begin  -- architecture rtl

  err  <= err_internal;
  data <= data_internal;

  rom1 : process (clk) is
  begin  -- process rom1
    if clk'event and clk = '1' then     -- rising clock edge
      if pc >= ROM_SIZE-1 then
        err_internal  <= '1';
        data_internal <= (others => '0');
      else
        err_internal  <= '0';
        data_internal <= mem(pc);
      end if;
    end if;
  end process rom1;

  process
    constant FILE_NAME : string := "data.bin";
    variable fstatus   : file_open_status;
    file fptr          : text;

    variable file_line         : line;
    variable data_internal_var : reg_t;
  begin

    file_open(fstatus, fptr, FILE_NAME, read_mode);

    for item in 0 to ROM_SIZE-1 loop
      if not endfile(fptr) then
        readline(fptr, file_line);
        bread(file_line, data_internal_var);
        mem(item) <= data_internal_var;
      else
        mem(item) <= (others => '0');
      end if;
    end loop;

    file_close(fptr);
    wait;
  end process;

end architecture rtl;
