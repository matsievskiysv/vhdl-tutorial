# Оперативная память

В цифровых схемах обширно используется оперативная память (**R**andom **A**ccess **M**emory, RAM). В реальных устройствах её начальное значение задаётся из постоянных запоминающих устройств. В данном проекте приведён пример инициализации оперативной памяти из текстового файла, что позволяет значительно упростить процедуру тестирования устройств, использующих оперативную память (ОЗУ). Например, при моделировании процессора данные и код будут записываться напрямую в ОЗУ.
