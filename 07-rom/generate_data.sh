#!/usr/bin/env bash

FILE=data.bin

rm -f $FILE
for i in $(seq 120); do
    for j in $(seq 32); do
	printf '%d' $((RANDOM % 2)) >> $FILE
    done
    printf '\n' >> $FILE
done
