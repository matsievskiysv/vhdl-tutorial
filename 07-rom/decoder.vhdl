library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.config.all;

entity decoder is

  port (
    clk    : in  std_ulogic;
    rst    : in  std_ulogic;
    input  : in  reg_t;
    output : out result_t);

end entity decoder;

architecture rtl of decoder is
  signal outp : result_t := (others => '0');
  signal in1  : result_t := (others => '0');
  signal in2  : result_t := (others => '0');
begin  -- architecture rtl

  output <= outp;
  in1  <= input(DATA_WIDTH/2-1 downto 0);
  in2  <= input(DATA_WIDTH-1 downto DATA_WIDTH/2);

  process (clk, rst) is
  begin  -- process out1
    if rst = '0' then                   -- asynchronous reset (active low)
      outp <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      outp <= std_ulogic_vector(unsigned(in1) + unsigned(in2));
    end if;
  end process;

end architecture rtl;
