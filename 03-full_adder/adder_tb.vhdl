library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top_tb is

end entity top_tb;

architecture top_tb of top_tb is
  constant TICK                   : time                          := 0.5 us;
  signal input                    : std_ulogic_vector(1 downto 0) := "00";
  signal carry_in, carry_out, sum : std_ulogic                    := '0';

  component full_adder is
    port (
      input_1, input_2, carry_in : in  std_ulogic;
      carry_out, sum             : out std_ulogic);
  end component full_adder;


begin  -- architecture top_tb

  adder1 : component full_adder
    port map (
      input_1   => input(0),
      input_2   => input(1),
      carry_in  => carry_in,
      carry_out => carry_out,
      sum       => sum);

  process is
  begin  -- process
    input    <= "00";
    carry_in <= '0';
    wait for TICK;
    assert (sum = '0' and carry_out = '0') report "Logic error" severity error;
    wait for TICK;

    input    <= "01";
    carry_in <= '0';
    wait for TICK;
    assert (sum = '1' and carry_out = '0') report "Logic error" severity error;
    wait for TICK;

    input    <= "10";
    carry_in <= '0';
    wait for TICK;
    assert (sum = '1' and carry_out = '0') report "Logic error" severity error;
    wait for TICK;

    input    <= "11";
    carry_in <= '0';
    wait for TICK;
    assert (sum = '0' and carry_out = '1') report "Logic error" severity error;
    wait for TICK;

    input    <= "00";
    carry_in <= '1';
    wait for TICK;
    assert (sum = '1' and carry_out = '0') report "Logic error" severity error;
    wait for TICK;

    input    <= "01";
    carry_in <= '1';
    wait for TICK;
    assert (sum = '0' and carry_out = '1') report "Logic error" severity error;
    wait for TICK;

    input    <= "10";
    carry_in <= '1';
    wait for TICK;
    assert (sum = '0' and carry_out = '1') report "Logic error" severity error;
    wait for TICK;

    input    <= "11";
    carry_in <= '1';
    wait for TICK;
    assert (sum = '1' and carry_out = '1') report "Logic error" severity error;
    wait for TICK;

  end process;


end architecture top_tb;
