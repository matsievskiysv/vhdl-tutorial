library ieee;
use ieee.std_logic_1164.all;

entity half_adder is
  port (
    input_1, input_2 : in  std_ulogic;
    sum, carry       : out std_ulogic);
end entity half_adder;

architecture rtl of half_adder is

begin  -- architecture rtl

  sum   <= input_1 xor input_2;
  carry <= input_1 and input_2;

end architecture rtl;

------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity full_adder is
  port (
    input_1, input_2, carry_in : in  std_ulogic;
    carry_out, sum             : out std_ulogic);
end entity full_adder;

architecture rtl of full_adder is
  signal carry1_internal, carry2_internal, sum_internal : std_ulogic;

begin  -- architecture rtl

  half_adder1 : entity work.half_adder(rtl)
    port map (
      input_1 => input_1,
      input_2 => input_2,
      carry   => carry1_internal,
      sum     => sum_internal);

  half_adder2 : entity work.half_adder(rtl)
    port map (
      input_1 => carry_in,
      input_2 => sum_internal,
      carry   => carry2_internal,
      sum     => sum);

  carry_out <= carry1_internal or carry2_internal;

end architecture rtl;

------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity four_bit_adder is
  port (
    num1, num2 : in  std_ulogic_vector(3 downto 0);
    sum        : out std_ulogic_vector(3 downto 0);
    carry      : out std_ulogic);
end entity four_bit_adder;

architecture rtl of four_bit_adder is

begin  -- architecture rtl

  -- заполнить

end architecture rtl;
