# Язык описания логических схем VHDL

## Арифметические операции

Для каждого типа данных в VHDL определены разные функции. К примеру, арифметические операции определены для типов `integer`, `signed` и `unsigned`, но не определены для `std_logic_vector`. Для произведения математических операций с аргументами типа `std_logic_vector` требуется сначала привести их к одному из типов, для которых эти операции определены. После выполнения операции, её результат преобразовывается обратно в тип `std_logic_vector`.

Для преобразования типа `std_logic_vector` к типам `signed` и `unsigned` можно воспользоваться командами
```vhdl
usgned_val <= unsigned(vec_val);
sgned_val <= signed(vec_val);
```

Для обратного преобразования можно воспользоваться командами
```vhdl
vec_val <= std_logic_vector(usgned_val);
vec_val <= std_logic_vector(sgned_val);
```

Полная диаграмма преобразований типов изображена на рисунке ниже
![conversion](images/vhdl-type-conversions.png)

В случае, когда количество бит исходном сигнале меньше, можно воспользоваться функцией `resize`
```vhdl
usgned_val <= resize(unsigned(vec_val), usgned_val'length);
```
или добавить требуемое количество бит вручную
```vhdl
usgned_val1 <= '0' & unsigned(vec_val);
usgned_val2 <= "010" & unsigned(vec_val);
```
В обратном случае, когда количество бит исходном сигнале больше, можно использовать индекс
```vhdl
vec_val <= std_logic_vector(usgned_val(3 downto 0));
```

## Условная генерация кода

Язык VHDL позволяет генерировать различный код на основе значения постоянных `generic`. Это позволяет изменять поведение добавляемых устройств без увеличения сложности генерируемой схемы.

Синтаксис условного кода следующий
```vhdl
label : if CONDITION_CONSTANT generate
  out <= sig1;
else generate
  out <= sig2;
end generate adder;
```

# Полу-сумматор и полный сумматор

Сумматор -- логический элемент, предназначенный для сложения двух цифровых сигналов.

## Полу-сумматор

Полу-сумматор имеет два входных сигнала и два выходных:

* `A`, `B`: складываемые сигналы
* `S`: сумма сигналов
* `Cout`: флаг переноса

| `A`  | `B`  | `S`  | `Cout`  |
|:-:|:-:|:-:|:-:|
| `0`  | `0`  | `0`  | `0`  |
| `0`  | `1`  | `1`  | `0`  |
| `1`  | `0`  | `1`  | `0`  |
| `1`  | `1`  | `0`  | `1`  |

## Полный сумматор

Полный сумматор отличается от полу-сумматор наличием дополнительного входа флага переноса. Благодаря этому каскадно соединённые полные сумматоры можно использовать для сложения бинарных чисел любого размера (по количеству используемых сумматоров). Часто для реализации схемы полного сумматора используют два полу-сумматора.

| `A`  | `B`  | `Cin`  | `S`  | `Cout`  |
|:-:|:-:|:-:|:-:|:-:|
| 0  | 0  | 0  | 0  | 0  |
| 0  | 0  | 1  | 1  | 0  |
| 0  | 1  | 0  | 1  | 0  |
| 0  | 1  | 1  | 0  | 1  |
| 1  | 0  | 0  | 1  | 0  |
| 1  | 0  | 1  | 0  | 1  |
| 1  | 1  | 0  | 0  | 1  |
| 1  | 1  | 1  | 1  | 1  |

# Задания

* Написать 4х битный сумматор из 4х полных сумматоров
* Написать полный сумматор в рамках одной архитектуры (не использовать полу-сумматоры)
* Написать компонент, складывающий два числа с количеством бит, заданным в параметре generic.
* Написать компонент, складывающий два числа с количеством бит, заданным в параметре generic. Тип чисел (с/без знака) также передаётся через параметр generic. Для реализации рекомендуется использовать конструкцию `if generate`.
