library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity unsigned_adder is
  generic (
    BIT_COUNT : integer);
  port (
    num1, num2 : in  std_ulogic_vector(BIT_COUNT-1 downto 0);
    sum        : out std_ulogic_vector(BIT_COUNT-1 downto 0);
    carry      : out std_ulogic);
end entity unsigned_adder;

architecture rtl of unsigned_adder is
  signal internal : unsigned(BIT_COUNT downto 0) := (others => '1');
begin  -- architecture rtl

  -- заполнить
  internal <= (others => '0');

end architecture rtl;

------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity signed_adder is
  generic (
    BIT_COUNT : integer);
  port (
    num1, num2 : in  std_ulogic_vector(BIT_COUNT-1 downto 0);
    sum        : out std_ulogic_vector(BIT_COUNT-1 downto 0);
    carry      : out std_ulogic);
end entity signed_adder;

architecture rtl of signed_adder is
  signal internal : signed(BIT_COUNT downto 0) := (others => '1');
begin  -- architecture rtl

  -- заполнить
  internal <= (others => '0');

end architecture rtl;

------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity generic_adder is
  generic (
    BIT_COUNT : integer;
    USIGNED   : boolean);
  port (
    num1, num2 : in  std_ulogic_vector(BIT_COUNT-1 downto 0);
    sum        : out std_ulogic_vector(BIT_COUNT-1 downto 0);
    carry      : out std_ulogic);
end entity generic_adder;

architecture rtl of generic_adder is

begin  -- architecture rtl

  adder : if USIGNED generate
    -- заполнить
  else generate
    -- заполнить
  end generate adder;

end architecture rtl;
