library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top_tb is

end entity top_tb;

architecture top_tb of top_tb is
  constant TICK                                   : time                                    := 0.5 us;
  constant BIT_COUNT                              : integer                                 := 4;
  signal input1, input2, sum_signed, sum_unsigned : std_ulogic_vector(BIT_COUNT-1 downto 0) := (others => '0');
  signal carry_signed, carry_unsigned             : std_ulogic                              := '0';


begin  -- architecture top_tb

  unsigned_adder1 : entity work.generic_adder(rtl)
    generic map (
      BIT_COUNT => BIT_COUNT,
      USIGNED   => true)
    port map (
      num1  => input1,
      num2  => input2,
      carry => carry_signed,
      sum   => sum_unsigned);

  signed_adder2 : entity work.generic_adder(rtl)
    generic map (
      BIT_COUNT => BIT_COUNT,
      USIGNED   => false)
    port map (
      num1  => input1,
      num2  => input2,
      carry => carry_unsigned,
      sum   => sum_signed);

  process is
    variable counter1, counter2 : integer range 0 to 2**BIT_COUNT-1 := 0;
  begin  -- process
    input1 <= std_ulogic_vector(to_unsigned(counter1, input1'length));
    input2 <= std_ulogic_vector(to_unsigned(counter2, input2'length));
    wait for TICK;
    if counter1 = 2**BIT_COUNT-1 then
      counter1 := 0;
      if counter2 = 2**BIT_COUNT-1 then
        counter2 := 0;
      else
        counter2 := counter2 + 1;
      end if;
    else
      counter1 := counter1 + 1;
    end if;
  end process;


end architecture top_tb;
