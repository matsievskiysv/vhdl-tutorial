library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity generic_adder is
  generic (
    BIT_COUNT : integer);
  port (
    num1, num2 : in  std_ulogic_vector(BIT_COUNT-1 downto 0);
    sum        : out std_ulogic_vector(BIT_COUNT-1 downto 0);
    carry      : out std_ulogic);
end entity generic_adder;

architecture rtl of generic_adder is
  signal internal : unsigned(BIT_COUNT downto 0) := (others => '1');
begin  -- architecture rtl

  -- заполнить
  internal <= (others => '0');

end architecture rtl;
