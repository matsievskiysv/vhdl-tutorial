library ieee;
use ieee.std_logic_1164.all;

entity full_adder is
  port (
    input_1, input_2, carry_in : in  std_ulogic;
    carry_out, sum             : out std_ulogic);
end entity full_adder;

architecture rtl of full_adder is

begin  -- architecture rtl

  -- заполнить

end architecture rtl;

------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity four_bit_adder is
  port (
    num1, num2 : in  std_ulogic_vector(3 downto 0);
    sum        : out std_ulogic_vector(3 downto 0);
    carry      : out std_ulogic);
end entity four_bit_adder;

architecture rtl of four_bit_adder is

begin  -- architecture rtl

  -- заполнить

end architecture rtl;
